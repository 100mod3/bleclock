import sys
from bluepy import btle

address = sys.argv[1]


def read_ble_values(mac):
    """
    Print all readable values for specified mac address.
    """
    perph = btle.Peripheral(mac)
    for service in perph.getServices():
        print('Service:', service.uuid)
        for chstcs in service.getCharacteristics():
            print('\tCharacteristic:', chstcs.uuid)
            print('\t\t', chstcs.propertiesToString())
            if chstcs.supportsRead():
                print('\t\t', chstcs.read())

if __name__ == '__main__':
    read_ble_values(address)
