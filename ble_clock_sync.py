import yaml
import logging
import sys
from time import time, altzone, timezone, daylight, localtime
from bluepy import btle


logger = logging.getLogger('ble_clock_sync.py')
handler = logging.StreamHandler()
formatter = logging.Formatter('%(levelname)s - %(name)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def get_timezone(use_custom_tz):
    """
    Return encoded binary timezone and timezone offset in seconds values
    derived from system timezone setting or passed :use_custom_tz: value.
    """
    if use_custom_tz:
        tz_offset_sec = - float(use_custom_tz) * 3600
        tz_binary = int(float(use_custom_tz)).to_bytes(1, byteorder='little', signed=True)
    else:
        tz_offset_sec = altzone if (localtime().tm_isdst > 0) else timezone
        tz_binary = int(- tz_offset_sec / 3600).to_bytes(1, byteorder='little', signed=True)
    return tz_offset_sec, tz_binary


def get_model_id(peripheral):
    """Return a model identifier of a peripheral object, or None."""
    model_id = None
    service = peripheral.getServiceByUUID('0000180a-0000-1000-8000-00805f9b34fb')
    for chstcs in service.getCharacteristics():
        if chstcs.uuid == '00002a24-0000-1000-8000-00805f9b34fb':
            if chstcs.supportsRead():
                model_id = chstcs.read()
                break
    return model_id.decode('utf-8') if model_id else None


def update_ble_time(mac_address, use_custom_tz):
    """
    Update current time and timezone information on a
    mac_address device in three connection attempts. Use UTC timezone
    and account offset into timestamp for specific scenarios.
    """
    tz_offset_sec, tz_binary = get_timezone(use_custom_tz)
    tz_is_whole_hour = (tz_offset_sec / 3600).is_integer()
    attempted = 0
    while attempted < 3:
        try:
            perph = btle.Peripheral(mac_address)
            servc = perph.getServiceByUUID('ebe0ccb0-7a0a-4b0c-8a1a-6ff2997da3a6')
            if get_model_id(perph) == 'LYWSD02' and tz_is_whole_hour:
                val = int(time()).to_bytes(4, byteorder='little') + tz_binary
            elif get_model_id(perph) in ['LYWSD02', 'MHO-C303']:
                val = int(time()-tz_offset_sec).to_bytes(4, byteorder='little') + b'\x00'
            else:
                logger.error('Unsupported device with MAC: %s', mac_address)
                break
            datetime = servc.getCharacteristics(forUUID='ebe0ccb7-7a0a-4b0c-8a1a-6ff2997da3a6')
            datetime[0].write(val)
            perph.disconnect()
            break
        except btle.BTLEDisconnectError as err:
            attempted += 1
            logger.info('%s; Attempt #%s', err, attempted)
        except:
            logger.error('Unexpected error: %s', sys.exc_info()[0])
            raise
    if attempted >= 3:
        logger.warning('Failed to update time on %s in 3 attempts', mac_address)


def main():
    if len(sys.argv) > 1:
        mac_address = sys.argv[1]
        if len(sys.argv) > 2:
            use_custom_tz = sys.argv[2]
        else:
            use_custom_tz = False
        update_ble_time(mac_address, use_custom_tz)
    else:
        with open(sys.path[0] + '/device_list.yaml') as file:
            try:
                device_list = yaml.safe_load(file)
            except yaml.YAMLError as exc:
                logger.error(exc)
        for mac_address, name in device_list.items():
            update_ble_time(mac_address, False)

if __name__ == '__main__':
    main()
